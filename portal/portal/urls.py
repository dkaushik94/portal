"""portal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url , patterns
from django.contrib import admin
from loginauth import urls as login_urls
from groups import urls as group_urls
from queries import urls as query_urls
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	url(r'^dashboard$','loginauth.views.homepage',name='dashboard'),
	url(r'^profile/(?P<username>\w+)/$','users.views.my_profile',name='profile'),
	url(r'^loginauth/',include(login_urls)),
	url(r'^groups/' , include(group_urls)),
	url(r'^query/' , include(query_urls)),
	url(r'^about_us$','users.views.about_us' , name = 'about_us'),
	url(r'^home$','users.views.home' , name = 'home'),
	url(r'^$','users.views.index' , name = 'index'),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
