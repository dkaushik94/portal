'''----------------------------------------------------------------------------------'''

#Django Related imports.
from django.db import models

'''----------------------------------------------------------------------------------'''

#Database related models.
from users.models import myuser

'''----------------------------------------------------------------------------------'''

class post(models.Model):
	created_timestamp = models.DateTimeField(auto_now_add = True)
	update_timstamp = models.DateTimeField(auto_now = True)
	title = models.CharField('Title' , max_length = 200 , blank = False)
	content = models.TextField()
	posting_user = models.ForeignKey(myuser , null = True , related_name = 'posting_user')
	followers = models.ManyToManyField(myuser , related_name = 'follows' , blank = True)

	def __str__(self):
		return self.title

	def followers_count(self):				#Returns number of likers. 
		return self.followers.count()

	
	# def likers(self):					#Returns all likers of a question. We can access thes users by writing 'self.likers.all()[i]'
	# 	return self.likers.all()
