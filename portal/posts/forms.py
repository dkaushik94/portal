'''---------------------------------------------------------------------------------------------------'''

#Django Related Imports
from django.forms import ModelForm

'''---------------------------------------------------------------------------------------------------'''

#Model Related Imports
from .models import post

'''---------------------------------------------------------------------------------------------------'''

#Miscellaneous Imports

'''---------------------------------------------------------------------------------------------------'''

#Form Class for post.
class group_post(ModelForm):
	def __init__(self, *args, **kwargs): 
		super(ModelForm, self).__init__(*args, **kwargs)
		self.fields['title'].label = 'What is the Resource about?'
		self.fields['content'].label = 'Write your content here. Try to post relevent links along with instructions.'
	class Meta: 
		model = post
		'''Required Fields'''
		fields = ['title' , 'content']

	