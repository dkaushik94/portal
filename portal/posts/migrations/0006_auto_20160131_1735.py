# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0005_post_posting_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='followers',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, blank=True, related_name='follows'),
        ),
    ]
