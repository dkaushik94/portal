'''----------------------------------------------------------------------------------'''

#Django Related imports.
from django import forms

'''----------------------------------------------------------------------------------'''

#Database model related models 
from users.models import myuser,teacher,student

'''----------------------------------------------------------------------------------'''




'''Signin form common for both teacher and student'''
class LoginForm(forms.Form):
	email = forms.EmailField()
	password = forms.CharField(widget=forms.PasswordInput)

	def clean_email(self):
		email = self.cleaned_data['email']
		if email and myuser.objects.filter(email=email).count==0:
			raise forms.ValidationError("No account exists for this email_id . Please first register on the site")
		return email		
	def clean_password(self):
		email = self.cleaned_data['email']
		password = self.cleaned_data.get("password")
		try:
			user = myuser.objects.get(email = email)
			if  user and  password  and not user.check_password(password):
				raise forms.ValidationError("Email or password is incorrect. Please try again!")	
		except myuser.DoesNotExist:
			raise forms.ValidationError("Email or password is incorrect. Please try again!")
		return password

'''Teacher signup form'''
class Teacher_signup_form(forms.Form):

	SHIFTS = (
		('morning','Morning'),
		('evening','Evening'),
	)
	
	BRANCH = (
			('cse','Computer Science & Engineering'),
			('ece','Electronics & Communication Engineering'),
			('it','Information Technology'),
			('eee','Electronics & Electrical Engineering'),
	)

	DESIGNATION = (
		('asst. prof','Asst. Professor'),
		('prof','Professor'),
		('reader' , 'reader'),
	)

	username = forms.CharField(max_length=50)
	password1 = forms.CharField(label = 'Password:',widget = forms.PasswordInput)
	password2 = forms.CharField(label = 'Confirm Password:',widget = forms.PasswordInput)
	first_name = forms.CharField(max_length=50)
	last_name = forms.CharField(max_length=50)
	email = forms.EmailField()
	contact_number = forms.CharField(max_length = 10)

	department = forms.ChoiceField(choices = BRANCH)
	shift = forms.ChoiceField(choices = SHIFTS )
	teacher_id = forms.CharField(max_length = 12)
	designation = forms.ChoiceField(choices = DESIGNATION)

	#checking for username
	def clean_username(self):
		username = self.cleaned_data['username']
			
		print(myuser.objects.filter(username = username).count())
		if username and myuser.objects.filter(username=username).count() != 0:
			raise forms.ValidationError("An account with this username already exists. Please choose another username")
		return username
	# checking if email already exists
	def clean_email(self):
		email = self.cleaned_data['email']
			
		print(myuser.objects.filter(email = email).count())
		if email and myuser.objects.filter(email = email).count() != 0:
			raise forms.ValidationError("An account with this email already exists. Choose Forgot password in case you have forgotten your password")
		return email
	#matching password1 and password2
	def clean_password2(self):
		password1 = self.cleaned_data['password1']
		password2 = self.cleaned_data['password2']		
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError("Passwords don't match")
		return password2

	
'''Student Signup Form'''
class Student_signup_form(forms.Form):

	SHIFTS = (
		('morning','Morning'),
		('evening','Evening'),
	)
	
	BRANCH = (
			('cse','Computer Science & Engineering'),
			('ece','Electronics & Communication Engineering'),
			('it','Information Technology'),
			('eee','Electronics & Electrical Engineering'),
	)

	username = forms.CharField(max_length=50)
	password1 = forms.CharField(label = 'Password:',widget = forms.PasswordInput)
	password2 = forms.CharField(label = 'Confirm Password:',widget = forms.PasswordInput)
	first_name = forms.CharField(max_length=50)
	last_name = forms.CharField(max_length=50)
	email = forms.EmailField()
	contact_number = forms.CharField(max_length = 10)
	department = forms.ChoiceField(choices = BRANCH)
	shift = forms.ChoiceField(choices = SHIFTS )
	enrollment_no = forms.CharField(max_length = 12)
	# tech_choice = forms.CharField(max_length = 50)

	#checking for username
	def clean_username(self):
		username = self.cleaned_data['username']
				
		print(myuser.objects.filter(username = username).count())
		if username and myuser.objects.filter(username = username).count() != 0:
			raise forms.ValidationError("An account with this username already exists. Please choose another username")
		return username
	# checking if email already exists
	def clean_email(self):
		email = self.cleaned_data['email']
			
		if email and myuser.objects.filter(email = email).count() != 0:
			raise forms.ValidationError("An account with this email already exists. Choose Forgot password in case you have forgotten your password")
		return email
	#matching password1 and password2
	def clean_password2(self):
		password1 = self.cleaned_data['password1']
		password2 = self.cleaned_data['password2']		
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError("Passwords don't match")


