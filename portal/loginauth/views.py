'''----------------------------------------------------------------------------------'''
#Django Related imports.
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.utils.encoding import force_bytes, force_text
from django.template import loader
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode,is_safe_url
from django.http import JsonResponse
'''----------------------------------------------------------------------------------'''

#Database related models.
from .models import *
from users.models import *
from groups.models import group

'''----------------------------------------------------------------------------------'''

#Forms and defined methods.
from .forms import *

'''----------------------------------------------------------------------------------'''
#Miscellaneous Import.
from loginauth.decorators import _is_active

'''----------------------------------------------------------------------------------''' 

'''DashBoard Render.'''
@login_required()
#@_is_active
def homepage(request):
	user = myuser.objects.get(username=request.user)
	if user.is_teacher is True:
		teacher_user = teacher.objects.filter(user = user)
		context = {
			'user' : user,
			'teacher' : teacher
		}
		return render(request, 'home/home_teacher.html',context)
	elif user.is_superuser:
		return redirect('/admin')	
	else:
		student_user = user.student_parent
		group = student_user.group_members.all()
		content = []
		for g in group:
			content += g.posts.all()
			content += g.doubts.all()
		content.sort(key=lambda x: x.update_timstamp, reverse=True)   #Inplace sorting of posts and queries according to timestamp
		context = {
			'user': user,
			'student' : student_user, 
			'groups' : group,
			'content' : content
		}
		return render(request, 'home/home_student.html',context)

'''Login View'''
def login_user(request):
	

	if request.user.is_authenticated():
		user = myuser.objects.get(email=request.user)
		return redirect('/profile/' + user.username)

	if request.method == 'POST':
		form = LoginForm(request.POST or None)
		if form.is_valid():
			
			email = form.cleaned_data.get('email')
			password = form.cleaned_data['password']	
			uname = myuser.objects.get(email=email).username

			user = authenticate(username=uname, password=password)


			if user is not None:
				login(request , user)
				if user.is_active:
					return redirect('/profile/' + user.username)
				else:
					print(request.user.is_authenticated())
					print(request.user.is_active)
					return HttpResponse("Your account is not active.")
			else:
				print('User is None')
				#return HttpResponse('No user with such details exists with us, please check the details and login again.')
				return render(request,'loginauth/login.html',{'form':form,'errors':form.errors})
		else:
			return render(request,'loginauth/login.html',{'form':form,'errors':form.errors})


	else:
		form = LoginForm()
		return render(request, 'loginauth/login.html',{'form':form,'errors':form.errors})


'''Logout'''
@login_required()
def logout_user(request):
	logout(request)
	return redirect('/')


'''View for Teacher Signup'''
def teacher_signup(request):
	if request.user.is_authenticated():
		return redirect("/")

	if request.method == 'POST':
		form = Teacher_signup_form(request.POST or None)
		if form.is_valid():
			username = form.cleaned_data['username']
			email = form.cleaned_data['email']
			password1 = form.cleaned_data['password1']
			password2 = form.cleaned_data['password2']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			contact_number = form.cleaned_data['contact_number']
			department = form.cleaned_data['department']
			shift = form.cleaned_data['shift']
			teacher_id = form.cleaned_data['teacher_id']
			designation = form.cleaned_data['designation']
			user = myuser()		#creating user instance
			teacher_user = teacher() 	#creating teacher instance
			user = myuser(username=username, email=email, first_name=first_name,last_name=last_name,\
					 contact_number=contact_number, shift=shift, department=department,\
					is_teacher=True)
			user.set_password(password1)
			user.is_active = False
			
			
			email_body_context = {			
				'name' : first_name,
				'username' : username,	
				'token': urlsafe_base64_encode(force_bytes(username))
			}

			body = loader.render_to_string('loginauth/activation_email.html',email_body_context)
			email_message = EmailMultiAlternatives("Portal | Account Activation" , body , 'verenkar.saksham@gmail.com', [email])
			email_message.send()
			user.save()
			teacher_user = teacher(user=user, teacher_id=teacher_id, designation=designation)
			teacher_user.save()
				
			return render(request,'loginauth/account_created.html',{'user':user})			
		else :
			return render(request,'loginauth/teachersignup.html',{'form':form,'errors':form.errors})

		return render(request,'loginauth/teachersignup.html',{'form':form,'errors':form.errors})
	else:
		form  = Teacher_signup_form()
		return render(request,'loginauth/teachersignup.html',{'form':form})

'''View for student signup	'''
def student_signup(request):
	if request.user.is_authenticated():
		return redirect("/")

	if request.method == 'POST':
		form = Student_signup_form(request.POST or None)
		if form.is_valid():
			print('form is valid')
			username = form.cleaned_data['username']
			password1 = form.cleaned_data['password1']
			password2 = form.cleaned_data['password2']
			email = form.cleaned_data['email']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			contact_number = form.cleaned_data['contact_number']
			department = form.cleaned_data['department']
			shift = form.cleaned_data['shift']
			enrollment_no = form.cleaned_data['enrollment_no']
			user = myuser()												#creating user instance
			student_user = student() 									#creating teacher instance
			user = myuser(username=username, email=email, first_name=first_name,last_name=last_name,\
					 contact_number=contact_number, shift=shift, department=department,\
					is_teacher=False)
			user.set_password(password1)
			user.is_active = False
			
			
			email_body_context = {			
				'name' : first_name,
				'username' : username,	
				'token': urlsafe_base64_encode(force_bytes(username))
			}

			body = loader.render_to_string('loginauth/activation_email.html',email_body_context)
			email_message = EmailMultiAlternatives("Portal | Account Activation" , body , 'verenkar.saksham@gmail.com', [email])
			email_message.send()
			user.save()
			student_user = student(user=user, enrollment_no=enrollment_no)
			student_user.save()	
			return render(request,'loginauth/account_created.html',{'user':user})			
		else :
			return render(request,'loginauth/studentsignup.html',{'form':form,'errors':form.errors})

		return render(request,'loginauth/studentsignup.html',{'form':form,'errors':form.errors})
	else:
		form  = Student_signup_form()
		return render(request,'loginauth/studentsignup.html',{'form':form})



'''Activation page redirection'''

@_is_active
@login_required()
def activate_user(request,uidb64):
	#if request.user.is_authenticated():
	#	return redirect('/profile')
	assert uidb64 is not None 
	try:
		uid = force_text(urlsafe_base64_decode(uidb64))
		user = myuser.objects.get(username=uid)
		if user.is_teacher == False:
			if request.method == 'GET':
				return render(request , 'loginauth/login_options.html')
			elif request.method == 'POST':
				first_group = request.POST['group1']
				first_group = group.objects.get(name = first_group)
				first_group.members = student.objects.filter(user = user)	#Adding student to selected group.
				if "group2" in request.POST:                         #Filtering group name from request object.
					second_group = request.POST['group2']						
					second_group = group.objects.get(name = second_group)
					second_group.members = student.objects.filter(user = user)
				else:
					print ("No second group selected.")
		else:
			return redirect('/loginauth/login')	
	except (TypeError, ValueError, OverflowError, myuser.DoesNotExist):
		return HttpResponse("User Does Not Exist")
	user.is_active = True
	print(user.is_active)
	print(user.email)
	user.save()
	return redirect('/loginauh/login')
