'''----------------------------------------------------------------------------------'''
#Django Related imports.
from django.shortcuts import render,redirect
'''----------------------------------------------------------------------------------'''

#Database related models.

'''----------------------------------------------------------------------------------'''

#Forms and defined methods.

'''----------------------------------------------------------------------------------'''
#Miscellaneous Import.

'''----------------------------------------------------------------------------------''' 

#Write decorators here.

#This returns the view function to be execute or redirects to homepage according to user.is_active being true or false.
def _is_active(x):
	def wrapper_view_function(request , *args , **kwargs):
		if request.user.is_active == False :
			return x(request , *args , **kwargs)
		else:
			return redirect('/')
	return wrapper_view_function
