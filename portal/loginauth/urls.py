from django.conf.urls import include , url

urlpatterns = [

    url(r'^login/$','loginauth.views.login_user', name = 'login'),
    url(r'^logout/$','loginauth.views.logout_user', name = 'logout'),
    url(r'^signup/teacher/$','loginauth.views.teacher_signup',name='teacher_signup'),
    url(r'^signup/student/$','loginauth.views.student_signup',name='student_signup'),
    url(r'activate/(?P<uidb64>[0-9A-Za-z_\-]+)/$','loginauth.views.activate_user', name='activate_user'),

]

