//Modal trigger function for about the group section
$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal({
    		opacity : 0.8,
    });
  });
$(document).ready(function(){
    $(".collapsible").collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
  });

function getMonth(mm){
	var mon = "null";
	switch (mm) {
    case 1:
        mon = "Jan";
        break;
    case 2:
        mon = "Feb";
        break;
    case 3:
        mon = "Mar";
        break;
    case 3:
        mon = "Apr";
        break;
    case 5:
        mon = "May";
        break;
    case 6:
        mon = "Jun";
        break;
    case 7:
        mon = "Jul";
        break;
    case 8:
    	mon = "Aug";
    	break;
    case 9:
        mon = "Sep";
        break;
    case 10:
        mon = "Oct";
        break;
    case 11:
        mon = "Nov";
        break;
    case 12:
    	mon = "Dec";
    	break;
   }
   return mon;
}

function getDate(timestamp){
	var date;
	var yy = timestamp.slice(0,4);
	var mm = parseInt(timestamp.slice(5,7));
	var dd = timestamp.slice(8,10);
	var h = timestamp.slice(11,13);
	var m = timestamp.slice(14,16);
	var a = 'a.m.';
	if(h>12){
		a = 'p.m.';
		h = h-12;
	}
	mon = getMonth(mm);
   date = mon + '. ' + dd + ', ' + yy + ', ' + h + ':' + m + ' ' + a; 
   return date
}

function showResourceSuccess(response){
	var j = response;
	var title = j[0].fields.title;
	var content = j[0].fields.content;
	var timestamp = getDate(j[0].fields.created_timestamp);
	$("<div class='z-depth-1 post background-white'><div class = 'image-url image-post'  style='background-image:url({{MEDIA_URL}}{{request.user.avatar}});'></div><h5><strong>"+ title +"</strong></h5><p >"+ content +"</p><p class = 'teal-text right'>"+ timestamp +"</p><button class='btn-floating waves-effect'>+</button></div>").hide().prependTo("#post_container").fadeIn('slow');
	$('#resource_form')[0].reset();
	$('label').removeClass('active');
}

function showResourceError(response){
	Materialize.toast("We couldn't post your Resource. Please try again!",4000);
}

function showDoubtsSuccess(response){
	var j = response;
	var title = j[0].fields.title;
	var content = j[0].fields.content;
	$("<div class='z-depth-1 post background-white'><div class = 'image-url image-post'></div><p>"+ content +"</p></div>").hide().prependTo("#doubt_container").fadeIn('slow');
	$('#doubt_form')[0].reset();
}

function showDoubtsError(response){
	Materialize.toast("We couldn't post your doubt. Please try again!",4000);
}

var flag = 0;
//AJAX request for posting a resource.
$('#resource_form').submit(function(e){
	e.preventDefault();
	var data = $(this).serializeArray() 
	var url = '/groups/'+$("#slug").val()+'/';
	//Form validation on the client side.
	if (($($("[name = 'title']")[0]).val() == "" || $($("[name = 'content']")[0]).val() == ""))
	{
		if(flag ==0)
		{
			$("<p class='error-text'>Please make sure the form is completely filled.</p>").appendTo('#resource_form');
			$(".error-text").addClass('errors');
			flag = 1;
		}
		else if(flag ==1)
		{
			$(".error-text").remove();
			$("<p class='error-text'>Please make sure the form is completely filled.</p>").appendTo('#resource_form');
			$(".error-text").addClass('errors');
		}
	}
	else
	{
		//AJAX call for the form.
		$.ajax({
			type : "POST",
			url : url,
			data : data,
			dataType: 'json',
			async : true, 
			success : function(response){
				showResourceSuccess(response)
			},
			error : function(response){
				showResourceError(response)
			}
		})
	}
});

//Posting a doubt AJAX request.
var flag1 = 0;
//AJAX request for posting a resource.
$('#doubt_form').submit(function(e){
	e.preventDefault();
	var data = $(this).serializeArray(); 
	var url = '/query/'+$("#slug").val()+'/';
	//Form validation on the client side.
	if ($($("[name='content']")[1]).val() == "")
	{
		if(flag1 ==0)
		{
			$("<p class='error-text'>Please make sure the form is completely filled.</p>").appendTo('#doubt_form');
			$(".error-text").addClass('errors');
			flag1 = 1;
		}
		else if(flag1 ==1)
		{
			$(".error-text").remove();
			$("<p class='error-text'>Please make sure the form is completely filled.</p>").appendTo('#doubt_form');
			$(".error-text").addClass('errors');
		}
	}
	else
	{
		//AJAX call for the form.
		$.ajax({
			type : "POST",
			url : url,
			data : data,
			dataType: 'json',
			async : true, 
			success : function(response){
				showDoubtsSuccess(response)
			},
			error : function(response){
				showDoubtsError(response)
			}
		})
	}
});
