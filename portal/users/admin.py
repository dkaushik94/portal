from django.contrib import admin
from .models import myuser,student,teacher

# Register your models here.

admin.site.register(myuser)
admin.site.register(student)
admin.site.register(teacher)

