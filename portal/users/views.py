'''--------------------------------------------------------------------------------------'''

#Django Related imports
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.template import RequestContext

'''--------------------------------------------------------------------------------------'''
#Database related models.
from .models import *
from users.models import *

'''----------------------------------------------------------------------------------'''


# Create your views here.

'''View For About us page'''
def about_us(request):
	return render(request , 'about_us/about_us.html')

'''View For Home page'''
def home(request):
	return render(request , 'home/home.html')

'''View For Index page'''
def index(request):
	if request.user.is_authenticated():
		return redirect('/dashboard')
	else:
		return render(request , 'index.html')

"""View for MyProfile Page"""

def my_profile(request,username):
	print(username)
	user=myuser.objects.get(username=username)
	context = {
		'user' : user,
	}
	return render(request , 'users/myprofile.html',context)
