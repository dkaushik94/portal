'''----------------------------------------------------------------------------------'''
#Django Related imports.
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
'''----------------------------------------------------------------------------------'''

#Database related models.

'''----------------------------------------------------------------------------------'''

#Choices for different fields.


#Model for teacher.
class myuser(AbstractUser):
	SHIFTS = (
		('morning','Morning'),
		('evening','Evening'),
	)
	BRANCH = (
			('cse','Computer Science & Engineering'),
			('ece','Electronics & Communication Engineering'),
			('it','Information Technology'),
			('eee','Electronics & Electrical Engineering'),
		)
	GENDER_CHOICES = (
			(None,'Gender'),
			('M','Male'),
			('F','Female'),
			('T','Transgender')
		)
	gender=models.CharField(_('Gender') , max_length=1 , choices=GENDER_CHOICES , blank=True)
	avatar = models.ImageField(_('Avatar'),upload_to = 'avatars/',blank=True)	
	department = models.CharField(max_length = 50 , choices = BRANCH , null = True)
	shift = models.CharField(max_length = 10 ,choices = SHIFTS , null = True)
	contact_number = models.CharField(max_length = 10 , null = True)
	is_teacher = models.BooleanField('Are you a teacher?' , default=False)

	def __str__(self):
		return self.username
	def get_avatar(self):
		if self.avatar:

			return '<img height="128px" width="128px" class="responsive-image circle" src="/media/%s" alt="UserImage">' % self.avatar
		else: 
			return '<img height="128px" width="128px" class="responsive-image circle" src="/media/avatars/default.png" alt="UserImage">'		

class teacher(models.Model):
	user = models.OneToOneField(myuser , related_name = 'teacher_parent')
	DESIGNATION = (
		('asst. prof','Asst. Professor'),
		('prof','Professor'),
		('reader','Reader'),
	)
	teacher_id = models.CharField(max_length = 12)
	designation = models.CharField(max_length = 20 , choices = DESIGNATION)
	is_hod = models.BooleanField(default = False)
	def __str__(self):
		return self.user.username


class student(models.Model):
	user = models.OneToOneField(myuser , related_name = 'student_parent')
	enrollment_no = models.CharField(max_length = 12)
	#tech_choice = models.CharField(max_length = 50)
	
	def __str__(self):
		return self.user.username

