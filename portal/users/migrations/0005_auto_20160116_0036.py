# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20160111_0102'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='avatar',
            field=models.ImageField(verbose_name='Avatar', blank=True, upload_to='avatars/'),
        ),
        migrations.AddField(
            model_name='myuser',
            name='gender',
            field=models.CharField(verbose_name='Gender', max_length=1, blank=True, choices=[(None, 'Gender'), ('M', 'Male'), ('F', 'Female'), ('T', 'Transgender')]),
        ),
    ]
