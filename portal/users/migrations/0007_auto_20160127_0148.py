# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20160116_0132'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='is_teacher',
            field=models.BooleanField(default=False, verbose_name='Are you a teacher?'),
        ),
    ]
