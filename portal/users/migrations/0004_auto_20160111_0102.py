# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20160104_2214'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='designation',
            field=models.CharField(max_length=20, choices=[('asst. prof', 'Asst. Professor'), ('prof', 'Professor'), ('reader', 'Reader')]),
        ),
    ]
