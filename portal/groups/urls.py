'''Relative URLs for APPNAME = "groups"'''

from django.conf.urls import url

urlpatterns = [

	url(r'(?P<group_name>[A-Za-z_\-]+)/$','groups.views.group_render',name = 'my_group'),

]
