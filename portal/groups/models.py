'''---------------------------------------------------------------------------------'''

#Django Related Imports
from django.db import models

'''---------------------------------------------------------------------------------'''

#Database model related imports.
from users.models import teacher , student
from queries.models import query
from posts.models import post
'''---------------------------------------------------------------------------------'''

#Class for groups.
class group(models.Model):

	#choices for parent groups
	TYPES_OF_GROUP= (
		('web_dev' , 'Web Development'),
		('mob_dev' , 'Mobile Application Development'), 
		('competitive_prog' , 'Competitive Programming'),
		('career_planning' , 'Higher Studies'),
		('ui_ux' , 'UI/UX Design')
	)

	name = models.CharField(max_length = 500)
	description = models.TextField(blank = True)
	members = models.ManyToManyField(student , symmetrical = False , related_name = 'group_members' , blank = True)
	mentors = models.ManyToManyField(student , symmetrical = False , related_name = 'group_mentors' , blank = True)
	admin = models.ManyToManyField(teacher , symmetrical = False , related_name = 'group_admins' , blank = True)
	posts = models.ManyToManyField(post , symmetrical = False , related_name = 'group_posts' , blank = True)
	doubts = models.ManyToManyField(query , symmetrical = False , related_name = 'group_doubts' , blank = True)
	display_pic = models.ImageField(upload_to = 'group_pics/' , blank = True) 
	type_of_group =  models.CharField(max_length = 30 , choices = TYPES_OF_GROUP)
	slug = models.SlugField(max_length = 100 , blank = True)


	def __str__(self):
		return self.name
