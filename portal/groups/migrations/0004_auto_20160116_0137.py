# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0003_auto_20160115_2352'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='admin',
            field=models.ManyToManyField(to='users.teacher', blank=True, related_name='group_admins'),
        ),
        migrations.AlterField(
            model_name='group',
            name='doubts',
            field=models.ManyToManyField(to='queries.query', blank=True, related_name='group_doubts'),
        ),
        migrations.AlterField(
            model_name='group',
            name='members',
            field=models.ManyToManyField(to='users.student', blank=True, related_name='group_members'),
        ),
        migrations.AlterField(
            model_name='group',
            name='mentors',
            field=models.ManyToManyField(to='users.student', blank=True, related_name='group_mentors'),
        ),
        migrations.AlterField(
            model_name='group',
            name='posts',
            field=models.ManyToManyField(to='posts.post', blank=True, related_name='group_posts'),
        ),
    ]
