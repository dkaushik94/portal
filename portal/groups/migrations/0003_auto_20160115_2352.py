# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0002_auto_20160111_0052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='admin',
            field=models.ManyToManyField(related_name='administrators', blank=True, to='users.teacher'),
        ),
        migrations.AlterField(
            model_name='group',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='group',
            name='doubts',
            field=models.ManyToManyField(related_name='posts2', blank=True, to='queries.query'),
        ),
        migrations.AlterField(
            model_name='group',
            name='members',
            field=models.ManyToManyField(related_name='students', blank=True, to='users.student'),
        ),
        migrations.AlterField(
            model_name='group',
            name='mentors',
            field=models.ManyToManyField(related_name='mentors', blank=True, to='users.student'),
        ),
        migrations.AlterField(
            model_name='group',
            name='posts',
            field=models.ManyToManyField(related_name='posts', blank=True, to='posts.post'),
        ),
    ]
