# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20160104_2214'),
        ('queries', '0001_initial'),
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='group',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=500)),
                ('description', models.TextField()),
                ('type_of_group', models.CharField(max_length=30, choices=[('web_dev', 'Web Development'), ('mob_dev', 'Mobile Application Development'), ('competitive_prog', 'Competitive Programming'), ('career_planning', 'Higher Studies'), ('ui_ux', 'UI/UX Design')])),
                ('admin', models.ManyToManyField(related_name='administrators', to='users.teacher')),
                ('doubts', models.ManyToManyField(related_name='posts', to='queries.query')),
                ('members', models.ManyToManyField(related_name='students', to='users.student')),
                ('mentors', models.ManyToManyField(related_name='mentors', to='users.student')),
                ('posts', models.ManyToManyField(related_name='posts', to='posts.post')),
            ],
        ),
    ]
