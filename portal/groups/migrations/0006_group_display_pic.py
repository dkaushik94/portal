# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('groups', '0005_group_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='display_pic',
            field=models.ImageField(blank=True, upload_to='/group_pics/'),
        ),
    ]
