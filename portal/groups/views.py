'''----------------------------------------------------------------------------------'''

#Django Related imports.
from django.shortcuts import render
from django.http import HttpResponse
from django.db import models
from django.contrib.auth.decorators import login_required
from django.core import serializers
'''----------------------------------------------------------------------------------'''

#Database related models.
from users.models import myuser
from .models import group
from posts.models import post
from queries.models import query

'''----------------------------------------------------------------------------------'''

#Form imports.
from posts.forms import group_post
from queries.forms import query_form

'''----------------------------------------------------------------------------------'''

# Create your views here.
@login_required()
def group_render(request , group_name):
	a,b=0,1
	group_name = group.objects.get(slug = group_name)
	resources = group_name.posts.order_by('created_timestamp').reverse()
	doubts = group_name.doubts.order_by('created_timestamp').reverse()
	if request.method == 'GET':
		form2 = query_form()
		if group_name.mentors.filter(user = request.user).count() != 0:
			form1 = group_post()
			a=1
			context = {'group' : group_name , 'form1' : form1 , 'form2' : form2 ,'a':a , 'b':b , 'resources' : resources , 'doubts' : doubts}
		else :
			context = {'group' : group_name , 'a':a , 'b':b , 'resources' : resources , 'form2' : form2 , 'doubts' : doubts}
		return render(request , 'groups/group.html' , context)
	elif request.method == 'POST':
			title = request.POST['title']
			content = request.POST['content']
			post_now = post.objects.create(title= title , content = content , posting_user  = request.user)
			group_name.posts.add(post_now)
			json_response = serializers.serialize('json', [ post_now ])
			return HttpResponse(json_response)

#Function for posting a data. Return json response for the latest doubt.
@login_required()
def query_post(request , group_name):
	if request.method == 'POST':
		content = request.POST['content']
		query_now = query.objects.create(content = content , query_poster  = request.user)
		group_name = group.objects.get(slug = group_name)
		group_name.doubts.add(query_now)
		json_response = serializers.serialize('json', [ query_now ])
		return HttpResponse(json_response)
	else:
		return HttpResponse("Request Denied.")
