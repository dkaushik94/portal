'''----------------------------------------------------------------------------------'''

#Django Related imports.
from django.db import models

'''----------------------------------------------------------------------------------'''

#Database related models.
from users.models import myuser

'''----------------------------------------------------------------------------------'''

class query(models.Model):
	created_timestamp = models.DateTimeField(auto_now_add = True)
	update_timstamp = models.DateTimeField(auto_now = True)
	content = models.TextField()
	likers = models.ManyToManyField(myuser , symmetrical = False , related_name = 'likesQ' , blank = True)
	query_poster = models.ForeignKey(myuser , related_name = 'query_poster' , null = True)
	
	def __str__(self):
		return self.content[:30]

	def likers_count(self):				#Returns number of likers. 
		return self.likers.count()
	
	def likers_all(self):					#Returns all likers of a question. We can access thes users by writing 'self.likers.all()[i]'
		return self.likers.all()
