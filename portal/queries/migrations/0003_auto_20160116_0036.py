# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('queries', '0002_query_followers'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='query',
            name='followers',
        ),
        migrations.RemoveField(
            model_name='query',
            name='likers',
        ),
        migrations.AddField(
            model_name='query',
            name='likers',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='likes'),
        ),
    ]
