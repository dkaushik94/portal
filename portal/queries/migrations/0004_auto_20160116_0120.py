# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('queries', '0003_auto_20160116_0036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='query',
            name='likers',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, related_name='likesQ'),
        ),
    ]
