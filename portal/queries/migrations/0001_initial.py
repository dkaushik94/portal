# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='query',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('created_timestamp', models.DateTimeField(auto_now_add=True)),
                ('update_timstamp', models.DateTimeField(auto_now=True)),
                ('content', models.TextField()),
                ('likers', models.IntegerField(default=0)),
            ],
        ),
    ]
