# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('queries', '0005_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='query',
            name='query_poster',
            field=models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, related_name='query_poster'),
        ),
    ]
