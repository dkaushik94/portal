'''---------------------------------------------------------------------------------------------------'''

#Django Related Imports
from django.forms import ModelForm

'''---------------------------------------------------------------------------------------------------'''

#Model Related Imports
from .models import query

'''---------------------------------------------------------------------------------------------------'''

#Miscellaneous Imports

'''---------------------------------------------------------------------------------------------------'''

#Form Class for query posting form.
class query_form(ModelForm):
	def __init__(self , *args ,**kwargs):
		super(ModelForm , self).__init__(*args , **kwargs)
		self.fields['content'].label = "What is your doubt?"
	class Meta:
		model = query
		fields = ['content']