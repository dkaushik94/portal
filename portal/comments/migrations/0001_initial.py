# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('queries', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('update_timestamp', models.DateTimeField(auto_now=True)),
                ('created_timestamp', models.DateTimeField(auto_now_add=True)),
                ('content', models.TextField()),
                ('commenter', models.ForeignKey(related_name='commenter', to=settings.AUTH_USER_MODEL)),
                ('parent_question', models.ForeignKey(related_name='parent_question', to='queries.query')),
            ],
        ),
        migrations.CreateModel(
            name='reply',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('update_timestamp', models.DateTimeField(auto_now=True)),
                ('created_timestamp', models.DateTimeField(auto_now_add=True)),
                ('content', models.TextField()),
                ('commentor', models.ForeignKey(related_name='commentor', to=settings.AUTH_USER_MODEL)),
                ('parent_comment', models.ForeignKey(related_name='parent_comment', to='comments.comment')),
            ],
        ),
    ]
