'''----------------------------------------------------------------------------------'''

#Django Related imports.
from django.db import models

'''----------------------------------------------------------------------------------'''

#Database related models.
from users.models import myuser
from queries.models import query

'''----------------------------------------------------------------------------------'''

#Class for comments.
class comment(models.Model):
	update_timestamp = models.DateTimeField(auto_now = True)
	created_timestamp = models.DateTimeField(auto_now_add = True)
	content = models.TextField()
	parent_question = models.ForeignKey(query , related_name = 'parent_question')
	commenter = models.ForeignKey(myuser , related_name = 'commenter')

	def __str__(self):
		return self.content[:30]

class reply(models.Model):
	update_timestamp = models.DateTimeField(auto_now = True)
	created_timestamp = models.DateTimeField(auto_now_add= True)
	content = models.TextField()
	parent_comment = models.ForeignKey(comment , related_name = 'parent_comment')
	commentor = models.ForeignKey(myuser , related_name = 'replier')

	def __str__(self):
		return self.content[:20]