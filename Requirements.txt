Django==1.8.5
Pillow==2.6.1
PyMySQL==0.6.7
django-easy-pjax==1.2.0
django-material==0.4.1
django-taggit==0.17.3
