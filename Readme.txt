Info about the project:

This is the project for the college portal.

To clone this repository keep the following things in mind :
1. Pull command will create a folder in the directory you run it in and initialise the repository inside that folder. So the directory will look like Portal>'repository pulled files>..'
2. Once you pull the repository , navigate inside the initial folder that was created and create your virtual environment,by following the following steps.

Initial Setup Guidelines post cloning repository:
1.Create Virtual environment:
	virtualenv -p /usr/bin/python3 DESTINATION_FOLDER_NAME (if not created creates automatically.)
	Initiate environment by "source venv/bin/activate"
2.Install all required packages with 'pip3 install -r Requirements.txt'. You have to be in the directory where this file has been provided.
3.After installation, add the app names: 'material.admin' in the first line under INSTALLED_APPS in settings.py and then add 'material'.
4.Runserver and check if your admin site is functioning properly or not.

--If any package is being installed by you which will be needed by other contributors,please run the command 'pip3 freeze > Requirements.txt' and then push the changes--
--Note: Please make sure your have the gitignore files in your git home. Otherwise ask and it will be provided. Dont upload your settings.py file and other such system specific files. This will pollute the repository and create unwanted efffects on every contributor.--  

Present contributing members:

-Debojit Kaushik - (email : kaushik.debojit@gmail.com)
-Saksham B.M Verenkar - (email : verenkar.saksham@gmail.com)
-Divyanshu Tomar - (email : tomar.divyanshu@gmail.com)
-Danish Kamal - (email : danish8802204230@gmail.com)
-Mayur Gupta - (email : mayurgpt07@gmail.com)


Any query can be asked directly to us. Happy Coding.
For any queries , mail any of us. Please avoid spamming.